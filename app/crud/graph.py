from typing import Optional
from app.db.session import AsyncSession
from sqlalchemy import select

from app.models.schemas import SchemaGraph, GraphWithId
from app.models.orm import Graph, Edge


async def create_graph(db: AsyncSession, graph: SchemaGraph) -> GraphWithId:
    """Create a graph in the database from a Pydantic SchemaGraph model"""
    graph_orm = Graph()

    edges = []
    for edge in graph.data:
        e = Edge(**edge.dict())
        edges.append(e)

    graph_orm.data = edges

    db.add(graph_orm)
    await db.commit()
    await db.refresh(graph_orm)
    return GraphWithId.from_orm(graph_orm)


async def get_graph(db: AsyncSession, graphId: int) -> Optional[GraphWithId]:
    """Get a graph from the database by its ID, return None if not found"""

    query = select(Graph).where(Graph.id == graphId)
    result = await db.execute(query)
    graph_orm = result.scalars().first()

    if graph_orm:
        return GraphWithId.from_orm(graph_orm)
