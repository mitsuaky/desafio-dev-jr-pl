from typing import AsyncGenerator
from app.db.session import async_session, AsyncSession


async def get_db() -> AsyncGenerator[AsyncSession, None]:
    """Async (fastapi) dependency to get a auto-closing async sqlalchemy session"""
    async with async_session() as session:
        yield session
