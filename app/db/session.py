from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
import os

# This will lead to a keyerror if the environment variable is not set,
# not allowing the application to run without a database.
db = os.environ["DATABASE_URL"]
if os.getenv("TESTING") == "1":
    # In case of testing, we need to use sqlite
    # Otherwise, we use postgres.
    db = os.getenv("TESTING_DATABASE_URL", "sqlite+aiosqlite:///test.db")

engine = create_async_engine(db, echo=False)
async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)
# expire_on_commit is set to False because we want to use the session asynchronously.
# https://docs.sqlalchemy.org/en/14/orm/extensions/asyncio.html#preventing-implicit-io-when-using-asyncsession
