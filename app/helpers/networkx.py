from typing import Optional, Union
import networkx as nx
from app.models.schemas import Distance, GraphWithId, Route, Routes, SchemaGraph


def schema_graph_to_networkx(schema_graph: Union[GraphWithId, SchemaGraph]) -> nx.Graph:
    """Convert a SchemaGraph (or GraphWithId) to a NetworkX Graph"""
    graph = nx.Graph()
    for edge in schema_graph.data:
        graph.add_edge(edge.source, edge.target, distance=edge.distance)
    return graph


def calculate_min_distance_between_towns(
    Graph: Union[GraphWithId, SchemaGraph], town1: str, town2: str
) -> Optional[Distance]:
    """Calculate the minimum distance between two towns
    if there is a path between them."""
    graph = schema_graph_to_networkx(Graph)
    if town1 not in graph.nodes or town2 not in graph.nodes:
        return None
    if town1 == town2:
        return Distance(distance=0, path=[town1])
    try:
        # https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.shortest_paths.weighted.single_source_dijkstra.html
        distance, path = nx.single_source_dijkstra(
            graph, town1, town2, weight="distance"
        )
        return Distance(distance=distance, path=path)  # type: ignore
    except nx.NetworkXNoPath:
        return Distance(distance=-1, path=[])


def calculate_all_simple_paths_between_towns(
    Graph: Union[GraphWithId, SchemaGraph],
    town1: str,
    town2: str,
    maxStops: Optional[int] = None,
) -> Optional[Routes]:
    """Calculate all shortest paths between two towns
    if there is a path between them."""
    graph = schema_graph_to_networkx(Graph)

    if town1 not in graph.nodes or town2 not in graph.nodes:
        return None

    if not nx.has_path(graph, town1, town2):
        return Routes(routes=[])

    # A single path can be found in O(V+E) time, but the number of simple paths
    # in a graph can be very large, e.g. O(n!) in the complete graph of order n.
    # https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.simple_paths.all_simple_paths.html
    path_generator = nx.all_simple_paths(graph, town1, town2, cutoff=maxStops)

    routes = []
    for path in path_generator:
        # path is a list with the nodes, so we transform it to a single string
        route = "".join(path)
        stops = len(path) - 1  # -1 because the first is the departure town
        routes.append(Route(route=route, stops=stops))

    return Routes(routes=routes)
