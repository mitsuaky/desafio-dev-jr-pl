from fastapi import FastAPI
from app.routers import distance, graph, routes

app = FastAPI()

app.include_router(distance.router)
app.include_router(graph.router)
app.include_router(routes.router)
