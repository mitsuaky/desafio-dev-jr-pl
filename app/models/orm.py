from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import declarative_base, declared_attr, relationship

base = declarative_base()


class AutoNamed:
    """Mixin for SQLAlchemy models that
    automatically name the tables based on the class name"""

    @declared_attr
    def __tablename__(cls) -> str:
        return cls.__name__.lower()  # type: ignore


class Graph(base, AutoNamed):
    """Graphs are a collection of edges"""

    id = Column(Integer, primary_key=True, autoincrement=True)

    # lazy="selectin" to set the relationship loading option to selectinload
    # to avoid IO when acessing the relationship atributes
    data: list["Edge"] = relationship("Edge", lazy="selectin")


class Edge(base, AutoNamed):
    """Edges are the connections between nodes (towns)"""

    source = Column(String(1), primary_key=True)
    target = Column(String(1), primary_key=True)
    distance = Column(Integer, nullable=False)
    graph_id = Column(Integer, ForeignKey("graph.id"), primary_key=True)
