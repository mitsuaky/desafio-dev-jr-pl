from pydantic import BaseModel

# "Abstract class" for when id is needed
class Id(BaseModel):
    id: int


# Graphs
class SchemaEdge(BaseModel):
    source: str
    target: str
    distance: int

    class Config:
        orm_mode = True


class SchemaGraph(BaseModel):
    data: list[SchemaEdge]

    class Config:
        orm_mode = True


class GraphWithId(SchemaGraph, Id):
    pass

    class Config:
        orm_mode = True


# Routes
class Route(BaseModel):
    route: str
    stops: int


class Routes(BaseModel):
    routes: list[Route]


# Distance
class Distance(BaseModel):
    distance: int
    path: list[str]
