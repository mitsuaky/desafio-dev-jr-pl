from app.crud.graph import get_graph
from app.db.deps import AsyncSession, get_db
from app.helpers.networkx import calculate_min_distance_between_towns
from app.models.schemas import Distance
from fastapi import Depends, HTTPException, status
from fastapi.routing import APIRouter
from starlette.concurrency import run_in_threadpool

router = APIRouter(prefix="/distance")


@router.post(
    "/{graphId}/from/{town1}/to/{town2}",
    response_model=Distance,
    responses={404: {"description": "Graph not found"}},
)
async def distance_between_towns(
    graphId: int, town1: str, town2: str, db: AsyncSession = Depends(get_db)
):
    graph = await get_graph(db, graphId)
    if graph is None:
        # Not found
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Graph not found"
        )

    # Make use of the starlette run_in_threadpool function to run the function in the internal threadpool
    # This is necessary because the function can be slow sometimes depending on the graph size
    # and we don't want to block the main event loop while waiting for the function to finish.
    distance = await run_in_threadpool(
        calculate_min_distance_between_towns, graph, town1, town2
    )

    # There is a chance that at leat one of the two towns don't exist in the graph,
    # as the instructions don't specify what to do in that case, i will just
    # return -1 with an empty path.
    return distance or Distance(distance=-1, path=[])
