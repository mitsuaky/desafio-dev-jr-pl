from fastapi.routing import APIRouter
from fastapi import Depends, HTTPException, status
from app.models.schemas import SchemaGraph, GraphWithId
from app.crud import graph
from app.db.session import AsyncSession
from app.db.deps import get_db

router = APIRouter(prefix="/graph")


@router.post(
    "",
    response_model=GraphWithId,
    status_code=201,
    response_description="Graph created",
)
async def post_graph(data: SchemaGraph, db: AsyncSession = Depends(get_db)):
    return await graph.create_graph(db, data)


@router.get(
    "/{graphId}",
    response_model=GraphWithId,
    responses={404: {"description": "Graph not found"}},
)
async def get_graph(graphId: int, db: AsyncSession = Depends(get_db)):
    result = await graph.get_graph(db, graphId)
    if result is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
    return result
