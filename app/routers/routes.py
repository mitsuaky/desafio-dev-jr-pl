from typing import Optional

from app.crud.graph import get_graph
from app.db.deps import AsyncSession, get_db
from app.helpers.networkx import calculate_all_simple_paths_between_towns
from app.models.schemas import Routes
from fastapi import Depends, HTTPException, status
from fastapi.routing import APIRouter
from starlette.concurrency import run_in_threadpool

router = APIRouter(prefix="/routes")


@router.post(
    "/{graphId}/from/{town1}/to/{town2}",
    response_model=Routes,
    responses={404: {"description": "Graph not found"}},
)
async def routes_between_towns(
    graphId: int,
    town1: str,
    town2: str,
    maxStops: Optional[int] = None,  # query parameter
    db: AsyncSession = Depends(get_db),
):
    graph = await get_graph(db, graphId)
    if graph is None:
        return HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="Graph not found"
        )

    # Make use of the starlette run_in_threadpool function to run the function in the internal threadpool
    # This is necessary because the function can be slow sometimes depending on the graph size
    # and we don't want to block the main event loop while waiting for the function to finish.
    routes = await run_in_threadpool(
        calculate_all_simple_paths_between_towns, graph, town1, town2, maxStops
    )

    # There is a chance that at leat one of the two towns don't exist in the graph,
    # as the instructions don't specify what to do in that case, i will just
    # return a routes with an empty list.
    return routes or Routes(routes=[])
