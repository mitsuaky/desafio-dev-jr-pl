import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_post_distance_between_towns(exemple_graph_input, client: AsyncClient):
    new_graph = await client.post("/graph", json=exemple_graph_input)
    id = new_graph.json()["id"]

    response = await client.post(f"distance/{id}/from/A/to/C")
    assert response.status_code == 200
    assert response.json()["distance"] == 9
    assert response.json()["path"] == ["A", "B", "C"]


@pytest.mark.asyncio
async def test_post_distance_between_same_town(
    exemple_graph_input, client: AsyncClient
):
    new_graph = await client.post("/graph", json=exemple_graph_input)
    id = new_graph.json()["id"]

    response = await client.post(f"distance/{id}/from/A/to/A")
    assert response.status_code == 200
    assert response.json()["distance"] == 0
    assert response.json()["path"] == ["A"]


@pytest.mark.asyncio
async def test_post_distance_between_towns_without_path(
    graph_input_w_isolated_nodes, client: AsyncClient
):
    new_graph = await client.post("/graph", json=graph_input_w_isolated_nodes)
    id = new_graph.json()["id"]

    response = await client.post(f"distance/{id}/from/A/to/E")
    assert response.status_code == 200
    assert response.json()["distance"] == -1
    assert response.json()["path"] == []

@pytest.mark.asyncio
async def test_post_distance_between_towns_not_in_graph(
    graph_input_w_isolated_nodes, client: AsyncClient
):
    new_graph = await client.post("/graph", json=graph_input_w_isolated_nodes)
    id = new_graph.json()["id"]

    response = await client.post(f"distance/{id}/from/A/to/E")
    assert response.status_code == 200
    assert response.json()["distance"] == -1
    assert response.json()["path"] == []


@pytest.mark.asyncio
async def test_post_distance_between_towns_not_found(client: AsyncClient):
    # -1 should never exist
    response = await client.post(f"distance/-1/from/A/to/E")
    assert response.status_code == 404
