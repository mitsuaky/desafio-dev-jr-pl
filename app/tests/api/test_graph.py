from httpx import AsyncClient
import pytest


@pytest.mark.asyncio
async def test_post_graph(client: AsyncClient, exemple_graph_input: dict):
    response = await client.post("/graph", json=exemple_graph_input)
    assert response.status_code == 201
    assert response.json()["id"] is not None
    assert response.json()["data"] == exemple_graph_input["data"]


@pytest.mark.asyncio
async def test_get_graph(client: AsyncClient, exemple_graph_input: dict):
    # get the graph id from the previous test
    response_post = await client.post("/graph", json=exemple_graph_input)

    response = await client.get(f"/graph/{response_post.json()['id']}")
    assert response.status_code == 200
    assert response.json()["id"] == response_post.json()["id"]
    assert response.json()["data"] == exemple_graph_input["data"]


@pytest.mark.asyncio
async def test_get_graph_not_found(client: AsyncClient):
    # -1 should never exist
    response = await client.get("/graph/-1")
    assert response.status_code == 404
