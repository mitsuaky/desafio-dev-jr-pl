import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_post_routes_between_towns(exemple_graph_input, client: AsyncClient):
    new_graph = await client.post("/graph", json=exemple_graph_input)
    id = new_graph.json()["id"]

    response = await client.post(f"routes/{id}/from/A/to/C")
    assert response.status_code == 200
    assert isinstance(response.json()["routes"], list)
    assert len(response.json()["routes"]) == 10
    assert response.json()["routes"][0]["route"] == "ABC"
    assert response.json()["routes"][0]["stops"] == 2


@pytest.mark.asyncio
async def test_post_routes_between_towns_without_path(
    graph_input_w_isolated_nodes, client: AsyncClient
):
    new_graph = await client.post("/graph", json=graph_input_w_isolated_nodes)
    id = new_graph.json()["id"]

    response = await client.post(f"routes/{id}/from/A/to/E")
    assert response.status_code == 200
    assert isinstance(response.json()["routes"], list)
    assert len(response.json()["routes"]) == 0


@pytest.mark.asyncio
async def test_post_routes_between_towns_not_in_graph(
    graph_input_w_isolated_nodes, client: AsyncClient
):
    new_graph = await client.post("/graph", json=graph_input_w_isolated_nodes)
    id = new_graph.json()["id"]

    response = await client.post(f"routes/{id}/from/A/to/E")
    assert response.status_code == 200
    assert isinstance(response.json()["routes"], list)
    assert len(response.json()["routes"]) == 0


@pytest.mark.asyncio
async def test_post_routes_between_towns_with_max_stops(
    exemple_graph_input, client: AsyncClient
):
    new_graph = await client.post("/graph", json=exemple_graph_input)
    id = new_graph.json()["id"]

    response = await client.post(f"routes/{id}/from/A/to/E", params={"maxStops": 3})
    assert response.status_code == 200
    assert isinstance(response.json()["routes"], list)
    assert len(response.json()["routes"]) == 4

    response = await client.post(f"routes/{id}/from/A/to/E", params={"maxStops": 2})
    assert len(response.json()["routes"]) == 2


@pytest.mark.asyncio
async def test_post_routes_between_towns_not_found(client: AsyncClient):
    # -1 should never exist
    response = await client.post(f"graph/-1/from/A/to/E")
    assert response.status_code == 404
