import asyncio
import os
from typing import AsyncGenerator

import pytest
import pytest_asyncio
from app.db.session import async_session, engine
from app.main import app
from app.models.orm import base
from httpx import AsyncClient

BASE_URL = "http://localhost:8000"


# Run before all tests, used to create the tables in the sqlite database
@pytest_asyncio.fixture(scope="session", autouse=True)
async def init_db():
    if os.getenv("TESTING") == "1":
        async with engine.begin() as conn:
            await conn.run_sync(base.metadata.create_all)
    else:
        raise Exception("TESTING environment variable is not 1 or not set")


@pytest.fixture(scope="session")
def event_loop():
    return asyncio.get_event_loop()


@pytest_asyncio.fixture
async def db() -> AsyncGenerator:
    """Async fixture to get a auto-closing sqlalchemy session"""
    async with async_session() as session:
        yield session


@pytest_asyncio.fixture(scope="session")
async def client() -> AsyncGenerator:
    async with AsyncClient(app=app, base_url=BASE_URL) as ac:
        yield ac


@pytest.fixture
def exemple_graph_input() -> dict:
    return {
        "data": [
            {"source": "A", "target": "B", "distance": 6},
            {"source": "A", "target": "E", "distance": 4},
            {"source": "B", "target": "A", "distance": 6},
            {"source": "B", "target": "C", "distance": 2},
            {"source": "B", "target": "D", "distance": 4},
            {"source": "C", "target": "B", "distance": 3},
            {"source": "C", "target": "D", "distance": 1},
            {"source": "C", "target": "E", "distance": 7},
            {"source": "D", "target": "B", "distance": 8},
            {"source": "E", "target": "B", "distance": 5},
            {"source": "E", "target": "D", "distance": 7},
        ]
    }


@pytest.fixture
def graph_input_w_isolated_nodes() -> dict:
    return {
        "data": [
            {"source": "A", "target": "B", "distance": 6},
            {"source": "B", "target": "C", "distance": 4},
            # ---
            {"source": "D", "target": "E", "distance": 6},
            {"source": "E", "target": "F", "distance": 2},
        ]
    }
