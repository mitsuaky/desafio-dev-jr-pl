from app.crud.graph import create_graph, get_graph
from app.models.orm import Edge
import pytest
from sqlalchemy.ext.asyncio import AsyncSession
from app.models.schemas import SchemaGraph, GraphWithId


@pytest.mark.asyncio
async def test_crud_create_graph(db: AsyncSession, exemple_graph_input) -> None:
    graph_in = SchemaGraph(**exemple_graph_input)
    graph = await create_graph(db, graph_in)

    assert graph.id is not None
    assert graph.data == exemple_graph_input["data"]


@pytest.mark.asyncio
async def test_crud_get_graph(db: AsyncSession, exemple_graph_input) -> None:
    graph_in = SchemaGraph(**exemple_graph_input)
    graph = await create_graph(db, graph_in)
    graph_out = await get_graph(db, graph.id)

    assert graph_out is not None
    assert graph_out.id == graph.id
    assert graph_out.data == graph.data


@pytest.mark.asyncio
async def test_crud_get_non_existing_graph(
    db: AsyncSession, exemple_graph_input
) -> None:

    # graph -1 should never exist
    graph_out = await get_graph(db, -1)

    assert graph_out is None
