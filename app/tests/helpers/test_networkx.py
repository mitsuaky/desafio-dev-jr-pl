from app.helpers.networkx import (
    calculate_min_distance_between_towns,
    calculate_all_simple_paths_between_towns,
)
from app.models.schemas import Distance, Route, Routes, SchemaGraph


def test_calculate_min_distance_between_towns(exemple_graph_input) -> None:
    graph = SchemaGraph(**exemple_graph_input)
    result = calculate_min_distance_between_towns(graph, "A", "B")

    assert result is not None
    assert isinstance(result, Distance)
    assert result.distance == 6
    assert result.path == ["A", "B"]


def test_calculate_min_distance_between_towns_without_path(
    graph_input_w_isolated_nodes,
) -> None:
    graph = SchemaGraph(**graph_input_w_isolated_nodes)
    result = calculate_min_distance_between_towns(graph, "A", "E")

    assert result is not None
    assert isinstance(result, Distance)
    assert result.distance == -1
    assert result.path == []


def test_calculate_min_distance_same_town(
    graph_input_w_isolated_nodes,
) -> None:
    graph = SchemaGraph(**graph_input_w_isolated_nodes)
    result = calculate_min_distance_between_towns(graph, "A", "A")

    assert result is not None
    assert isinstance(result, Distance)
    assert result.distance == 0
    assert result.path == ["A"]


def test_calculate_min_distance_between_towns_not_in_graph(
    graph_input_w_isolated_nodes,
) -> None:
    graph = SchemaGraph(**graph_input_w_isolated_nodes)
    result = calculate_min_distance_between_towns(graph, "A", "W")

    assert result is None


def test_calculate_all_simple_paths_between_towns(exemple_graph_input) -> None:
    graph = SchemaGraph(**exemple_graph_input)
    result = calculate_all_simple_paths_between_towns(graph, "A", "B")

    assert result is not None
    assert isinstance(result, Routes)
    assert isinstance(result.routes[0], Route)
    assert len(result.routes) == 6
    assert result.routes[0].route == "AB"
    assert result.routes[0].stops == 1
    assert result.routes[1].route == "AECB"
    assert result.routes[1].stops == 3
    assert result.routes[2].route == "AECDB"
    assert result.routes[2].stops == 4


def test_calculate_all_simple_paths_between_towns_with_maxstop(
    exemple_graph_input,
) -> None:
    graph = SchemaGraph(**exemple_graph_input)
    result = calculate_all_simple_paths_between_towns(graph, "A", "B", maxStops=3)

    assert result is not None
    assert isinstance(result, Routes)
    assert isinstance(result.routes[0], Route)
    assert len(result.routes) == 4
    assert result.routes[0].route == "AB"
    assert result.routes[0].stops == 1
    assert result.routes[1].route == "AECB"
    assert result.routes[1].stops == 3
    assert result.routes[2].route == "AEB"
    assert result.routes[2].stops == 2


def test_calculate_all_simple_paths_between_towns_without_path(
    graph_input_w_isolated_nodes,
) -> None:
    graph = SchemaGraph(**graph_input_w_isolated_nodes)
    result = calculate_all_simple_paths_between_towns(graph, "A", "E")

    assert result is not None
    assert isinstance(result, Routes)
    assert result.routes == []
    assert len(result.routes) == 0
